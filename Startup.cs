using Bg.PruebaTecnica.Filter;
using Bg.PruebaTecnica.Repositorio.Config;
using Bg.PruebaTecnica.Repositorio.Implementacion;
using Bg.PruebaTecnica.Repositorio.Interface;
using Bg.PruebaTecnica.Servicio.Implementacion;
using Bg.PruebaTecnica.Servicio.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddScoped<AutenticationAuthorizeAttribute>();
            services.AddScoped<ExceptionFilter>();
            services.AddScoped<ITokenServicio, TokenServicio>();
            services.AddScoped<ICarritoServicio, CarritoServicio>();
            services.AddScoped<ICarritoRepositorio, CarritoRepositorio>();

            services.AddDbContext<CarritoConfig>(options =>
                       options.UseSqlServer(Configuration.GetConnectionString("bg")));

            //services.AddCors(options =>
            //{
            //    options.AddPolicy("Policy1",
            //         builder =>
            //         {

            //           .builder.WithOrigins("http://localhost:4200")
            //            .AllowAnyHeader()
            //            .AllowAnyMethod();
            //         });
            //});
            //services.AddCors(options =>
            //{
            //    options.AddPolicy(name: "Policy1",
            //                      policy =>
            //                      {

            //                          policy.WithOrigins("http://localhost:4200",
            //                                              "http://www.contoso.com");
            //                          policy.AllowAnyHeader();
            //                          policy.AllowAnyMethod();
            //                      });
            //});

            services.AddCors();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Bg.PruebaTecnica", Version = "v1" });
            });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Bg.PruebaTecnica v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(x => x
               .AllowAnyMethod()
               .AllowAnyHeader()
               .SetIsOriginAllowed(origin => true) // allow any origin
               .AllowCredentials()); // allow credentials


            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
