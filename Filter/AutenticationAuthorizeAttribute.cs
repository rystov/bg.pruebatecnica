using Bg.PruebaTecnica.Model;
using Bg.PruebaTecnica.Servicio.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Filter
{
    public class AutenticationAuthorizeAttribute : ActionFilterAttribute
    {
        ITokenServicio _ser;
        public AutenticationAuthorizeAttribute(ITokenServicio ser)
        {
            _ser = ser;
        }


        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = filterContext.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.ToUpper() == "JWT").Value + "";
            if (string.IsNullOrEmpty(user))
            {
                // not logged in
                var d = new ResponseErrorType() { Codigo = "api_no_autorizada", Detalle = "token no vigente o no autenticado" };
                filterContext.Result = new JsonResult(new { d }) { StatusCode = StatusCodes.Status401Unauthorized };
            }
            else
            
            {

                if (!_ser.VerificoJWT(user))
                {
                    var d = new ResponseErrorType() { Codigo = "api_no_autorizada", Detalle = "token no vigente o no autenticado" };
                    filterContext.Result = new JsonResult(new { d }) { StatusCode = StatusCodes.Status401Unauthorized };

                }
            }

        }

    }
}
