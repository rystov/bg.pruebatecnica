﻿using Bg.PruebaTecnica.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Filter
{
    public class ExceptionFilter : IExceptionFilter
    {

        public void OnException(ExceptionContext context)
        {

            var d = new ResponseErrorType() { Codigo = "api_error_fatal", Detalle = "token no vigente o no autenticado" };
            context.Result = new JsonResult(new { d }) { StatusCode = StatusCodes.Status500InternalServerError };

        }
    }
}
