﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Repositorio.Dto
{
    public class UsuarioDTO
    {
        public int IdUsuario { get; set; }


        public string NombreP { get; set; }
        public string NombreS { get; set; }
        public string ApellidoP { get; set; }
        public string ApellidoS { get; set; }
        public string Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Username { get; set; }
        public string IsLoginCorrecto { get; set; }


    }
}
