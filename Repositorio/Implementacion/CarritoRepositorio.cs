using Bg.PruebaTecnica.Repositorio.Config;
using Bg.PruebaTecnica.Repositorio.Dto;
using Bg.PruebaTecnica.Repositorio.Interface;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Repositorio.Implementacion
{
    public class CarritoRepositorio : ICarritoRepositorio
    {
        private readonly CarritoConfig _db;
        public CarritoRepositorio(CarritoConfig Context)
        {
            this._db = Context;

        }
        public UsuarioDTO Usuario(string User, string Pwd)
        {

            var PUsr = new SqlParameter("@Usr", User);
            var PPwd = new SqlParameter("@Pwd", Pwd);

            var RstUsaurio = _db
                       .Usuario
                       .FromSqlRaw(
                        string.Concat("exec ", "[CarritoCompras].[ConsultaUsuario]", " @Usr,@Pwd"),
                        PUsr, PPwd
                        )
                      .AsEnumerable().FirstOrDefault();

            return RstUsaurio;
        }



    }
}
