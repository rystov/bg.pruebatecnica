using Bg.PruebaTecnica.Repositorio.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Repositorio.Interface
{
    public interface ICarritoRepositorio
    {
        public UsuarioDTO Usuario(string User, string Pwd);
    }
}
