using Bg.PruebaTecnica.Repositorio.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Repositorio.Config
{
  public class CarritoConfig : DbContext
  {

    public CarritoConfig(DbContextOptions<CarritoConfig> options)
       : base(options)
    {
    }
        public DbSet<UsuarioDTO> Usuario { get; set; }

    }
}
