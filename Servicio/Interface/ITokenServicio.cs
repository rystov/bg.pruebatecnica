﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Servicio.Interface
{
    public interface ITokenServicio

    {
        public string GeneroJWT(string User);
        public bool VerificoJWT(string Token);
    }
}
