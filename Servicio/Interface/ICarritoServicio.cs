using Bg.PruebaTecnica.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Servicio.Interface
{
    public interface ICarritoServicio
    {
        public UsuarioOutput Usuario(string User, string Pwd);
    }
}
