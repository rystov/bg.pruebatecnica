using Bg.PruebaTecnica.Model;
using Bg.PruebaTecnica.Repositorio.Interface;
using Bg.PruebaTecnica.Servicio.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Servicio.Implementacion
{
    public class CarritoServicio : ICarritoServicio
    {
        ICarritoRepositorio _repo;
        public CarritoServicio(ICarritoRepositorio repo)
        {
            _repo = repo;

        }
        public UsuarioOutput Usuario(string User, string Pwd)
        {
            var Repos = _repo.Usuario(User, Pwd);

            return new UsuarioOutput() { ApellidoP = Repos.ApellidoP, ApellidoS = Repos.ApellidoS, Detalle = "", Estado = Repos.Estado, FechaCreacion = Repos.FechaCreacion, NombreP = Repos.NombreP };

        }
    }
}
