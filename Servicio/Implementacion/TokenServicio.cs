﻿using Bg.PruebaTecnica.Servicio.Interface;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Servicio.Implementacion
{
    public class TokenServicio : ITokenServicio
    {

        public string GeneroJWT(string User)
        {

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("LUEGOCAMBIO?AQUIVAUNACLAVE SECRETA JAJAJAJA.");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("UserName", User) }),
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        public bool VerificoJWT(string Token)
        {

            try
            {
                var TokenHan = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("LUEGOCAMBIO?AQUIVAUNACLAVE SECRETA JAJAJAJA.");
                TokenHan.ValidateToken(Token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken ValicacionToken);

                var JWT = (JwtSecurityToken)ValicacionToken;
                var UserId = JWT.Claims.First(x => x.Type == "UserName").Value;
                return !string.IsNullOrEmpty(UserId);
            }
            catch (Exception e)
            {
                return false;
            }

        }

    }
}
