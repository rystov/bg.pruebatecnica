﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Model
{
    public class ProductoOutput
    {
		public Guid? IdProducto { get; set; }
		
		public string DescripcionProducto { get; set; }
		public string DescripcionCategoria { get; set; }
		public string Detalle { get; set; }
		public string Imagen { get; set; }
		public int IdCategoria { get; set; }
		public Decimal Precio { get; set; }
		public DateTime? FechaCreacion { get; set; }
		public DateTime? FechaVigente { get; set; }
		public string Estado { get; set; }


	
			
	}
}

