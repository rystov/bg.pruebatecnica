﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Model
{
    public class ResponseErrorType
    {
        public string Codigo { get; set; }
        public string Detalle { get; set; }
    }
}
