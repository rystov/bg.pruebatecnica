﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Model
{
    public class AutenticacionOutput
    {
        public UsuarioOutput Usuario { get; set; }

        public string Jwt { get; set; }
        public DateTime FechaExpira { get; set; }
    }
}
