﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Model
{
    public class TransaccionInput
    {
        public Guid? Id { get; set; }
        public int IdProducto { get; set; }
        public string IdUsuario { get; set; }
        public Decimal Precio { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string Estado { get; set; }


    }
}
