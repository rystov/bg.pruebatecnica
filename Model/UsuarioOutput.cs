﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Model
{
    public class UsuarioOutput
    {
		public string Detalle { get; set; }
		public string NombreP { get; set; }
		public string NombreS { get; set; }
		public string ApellidoP { get; set; }
		public string ApellidoS { get; set; }
		public string Estado { get; set; }
		public DateTime FechaCreacion { get; set; }
		public string Username { get; set; }

	
	}
}
