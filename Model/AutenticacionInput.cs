﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Model
{
    public class AutenticacionInput
    {
        public string Usuario { get; set; }
        public string Contrasenia { get; set; }
    }
}
