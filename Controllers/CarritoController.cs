using Bg.PruebaTecnica.Filter;
using Bg.PruebaTecnica.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Controllers
{

    [Route("online-tienda/v1/")]
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    [ServiceFilter(typeof(AutenticationAuthorizeAttribute))]
    [ServiceFilter(typeof(ExceptionFilter))]
    public class CarritoController : Controller
    {
        /// <summary>
        ///  
        /// </summary>
        /// <returns>Devuelve HTTP 200</returns>
        /// <response code="200">Consulta Exitosa </response>
        /// <response code="400" ></response> 
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseErrorType), StatusCodes.Status400BadRequest)]
        [HttpPost("transacciones")]
        public virtual IActionResult PostTrx([FromBody] List<TransaccionInput> request, [FromHeader] string JWT)
        {



            return Ok(1);
        }

        /// <summary>
        ///  
        /// </summary>
        /// <returns>Devuelve HTTP 200</returns>
        /// <response code="200">Consulta Exitosa </response>
        /// <response code="400" ></response> 
        [ProducesResponseType(typeof(List<ProductoOutput>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseErrorType), StatusCodes.Status400BadRequest)]
        [HttpGet("productos")]
        
        public virtual IActionResult GetPoductos([FromRoute] Guid request, [FromHeader] string JWT)
        {



            return Ok(1);
        }
    }
}
