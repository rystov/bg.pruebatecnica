using Bg.PruebaTecnica.Filter;
using Bg.PruebaTecnica.Model;
using Bg.PruebaTecnica.Servicio.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bg.PruebaTecnica.Controllers
{
    [Route("autenticacion/v1/")]
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    [ServiceFilter(typeof(ExceptionFilter))]
    public class AutenticacionController : Controller
    {

        ITokenServicio _ser;
        public AutenticacionController(ITokenServicio ser)
        {
            _ser = ser;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <returns>Devuelve HTTP 200</returns>
        /// <response code="200">Consulta Exitosa </response>
        /// <response code="400" ></response> 
        [ProducesResponseType(typeof(AutenticacionOutput), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseErrorType), StatusCodes.Status400BadRequest)]
        [HttpPost("access-token")]
        public virtual IActionResult AccessToken([FromBody] AutenticacionInput request)
        {



            return Ok(_ser.GeneroJWT(request.Usuario));
        }


    }
}
